/**
 * Created by alex on 16.12.2016.
 */
'use strict';

let expect = require('chai').expect;
let encodeUriForFlash = require('../lib/index').encodeUriForFlash;
let decodeUriForFlash = require('../lib/index').decodeUriForFlash;

const UPPOD_LINK = 'http://uppod.ru';
const KEY = 'Qy2fNM7ZW4ptNxmUWdIu';
describe('Flash coder', function () {
    describe('Encoder', function () {
        it('should encode links without key', function () {
            expect(encodeUriForFlash(UPPOD_LINK)).is.equal('Qy2fNM7ZW4ptNxmUWdIu')
        });
        it('should encode links with key', function () {
            expect(encodeUriForFlash(UPPOD_LINK, KEY)).is.equal('Qy2fkYUHvUbccuVYcygfzdhwppnUSYkFNM7ZW4ptNxmUWdIu')
        });
    })
    describe('Decoder', function () {
        it('should decode links without key', function () {
            expect(decodeUriForFlash(encodeUriForFlash(UPPOD_LINK))).is.equal(UPPOD_LINK)
        });
        it('should decode links with key', function () {
            expect(decodeUriForFlash(encodeUriForFlash(UPPOD_LINK, KEY), KEY)).is.equal(UPPOD_LINK)
        });
    })
})