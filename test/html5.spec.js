"use strict";

let expect = require('chai').expect;
let encodeUriForHtml5 = require('../lib/index').encodeUriForHtml5;
let decodeUriForHtml5 = require('../lib/index').decodeUriForHtml5;

describe('Html5 coder', function () {
    describe('Encoder', function () {

        it("should encode empty string", function () {
            expect(encodeUriForHtml5("")).to.be.equal("#");
        });

        it("should encode given special symbol", function () {
            expect(encodeUriForHtml5("-")).to.be.equal("#02d");
            expect(encodeUriForHtml5("/")).to.be.equal("#02f");
            expect(encodeUriForHtml5("_")).to.be.equal("#05f");
        });

        it("should encode given numeric symbol", function () {
            expect(encodeUriForHtml5("0")).to.be.equal("#030");
            expect(encodeUriForHtml5("9")).to.be.equal("#039");
        });

        it("should encode given ASCII CAPITAL LETTER", function () {
            expect(encodeUriForHtml5("S")).to.be.equal("#053");
            expect(encodeUriForHtml5("H")).to.be.equal("#048");
            expect(encodeUriForHtml5("I")).to.be.equal("#049");
            expect(encodeUriForHtml5("T")).to.be.equal("#054");
        });

        it("should encode given non-ASCII CAPITAL LETTER", function () {
            expect(encodeUriForHtml5("Б")).to.be.equal("#411");
            expect(encodeUriForHtml5("Л")).to.be.equal("#41b");
            expect(encodeUriForHtml5("Я")).to.be.equal("#42f");
        });

        it("should encode given ASCII small letter", function () {
            expect(encodeUriForHtml5("s")).to.be.equal("#073");
            expect(encodeUriForHtml5("h")).to.be.equal("#068");
            expect(encodeUriForHtml5("i")).to.be.equal("#069");
            expect(encodeUriForHtml5("t")).to.be.equal("#074");
        });

        it("should encode given non-ASCII small letter", function () {
            expect(encodeUriForHtml5("б")).to.be.equal("#431");
            expect(encodeUriForHtml5("л")).to.be.equal("#43b");
            expect(encodeUriForHtml5("я")).to.be.equal("#44f");
        });

        it("should encode given string", function () {
            var example = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-";
            expect(encodeUriForHtml5(example)).to.be.equal("#03003103203303403503603703803906106206306406506606706806906a06b06c06d06e06f07007107207307407507607707807907a04104204304404504604704804904a04b04c04d04e04f05005105205305405505605705805905a05f02d");
            expect(encodeUriForHtml5(example.split("").reverse().join(""))).to.be.equal("#02d05f05a05905805705605505405305205105004f04e04d04c04b04a04904804704604504404304204107a07907807707607507407307207107006f06e06d06c06b06a069068067066065064063062061039038037036035034033032031030");
        });
    });

    describe("Decoder", function() {
        let errMsg = "Invalid encoded string";

        it("should throw error for invalid string", function() {
            expect(decodeUriForHtml5.bind(this, "")).to.throw(Error, errMsg + ": should not be empty");
            expect(decodeUriForHtml5.bind(this, "0123")).to.throw(Error, errMsg + ": should start with '#' character");
            expect(decodeUriForHtml5.bind(this, "#0123")).to.throw(Error, errMsg + ": should have length divided by 3");
        });

        it("should decode empty string", function () {
            expect(decodeUriForHtml5("#")).to.be.equal("");
        });

        it("should decode given special symbol", function () {
            expect(decodeUriForHtml5("#02d")).to.be.equal("-");
            expect(decodeUriForHtml5("#02f")).to.be.equal("/");
            expect(decodeUriForHtml5("#05f")).to.be.equal("_");
        });

        it("should decode given numeric symbol", function () {
            expect(decodeUriForHtml5("#030")).to.be.equal("0");
            expect(decodeUriForHtml5("#039")).to.be.equal("9");
        });

        it("should decode given ASCII CAPITAL LETTER", function () {
            expect(decodeUriForHtml5("#053")).to.be.equal("S");
            expect(decodeUriForHtml5("#048")).to.be.equal("H");
            expect(decodeUriForHtml5("#049")).to.be.equal("I");
            expect(decodeUriForHtml5("#054")).to.be.equal("T");
        });

        it("should decode given non-ASCII CAPITAL LETTER", function () {
            expect(decodeUriForHtml5("#411")).to.be.equal("Б");
            expect(decodeUriForHtml5("#41b")).to.be.equal("Л");
            expect(decodeUriForHtml5("#42f")).to.be.equal("Я");
        });

        it("should decode given ASCII small letter", function () {
            expect(decodeUriForHtml5("#073")).to.be.equal("s");
            expect(decodeUriForHtml5("#068")).to.be.equal("h");
            expect(decodeUriForHtml5("#069")).to.be.equal("i");
            expect(decodeUriForHtml5("#074")).to.be.equal("t");
        });

        it("should decode given non-ASCII small letter", function () {
            expect(decodeUriForHtml5("#431")).to.be.equal("б");
            expect(decodeUriForHtml5("#43b")).to.be.equal("л");
            expect(decodeUriForHtml5("#44f")).to.be.equal("я");
        });

        it("should decode given string", function () {
            var str = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-";
            var example = "#03003103203303403503603703803906106206306406506606706806906a06b06c06d06e06f07007107207307407507607707807907a04104204304404504604704804904a04b04c04d04e04f05005105205305405505605705805905a05f02d";
            expect(decodeUriForHtml5(example)).to.be.equal(str);
            var example2 = "#02d05f05a05905805705605505405305205105004f04e04d04c04b04a04904804704604504404304204107a07907807707607507407307207107006f06e06d06c06b06a069068067066065064063062061039038037036035034033032031030";
            expect(decodeUriForHtml5(example2)).to.be.equal(str.split("").reverse().join(""));
        });

    });
});