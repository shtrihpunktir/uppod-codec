/**
 * Created by alex on 15.12.2016.
 */
'use strict';

var encodeUriForFlash = require('./flash').encode;
var decodeUriForFlash = require('./flash').decode;
var encodeUriForHtml5 = require('./html5').encode;
var decodeUriForHtml5 = require('./html5').decode;

module.exports = {
    encodeUriForFlash: encodeUriForFlash,
    decodeUriForFlash: decodeUriForFlash,
    encodeUriForHtml5: encodeUriForHtml5,
    decodeUriForHtml5: decodeUriForHtml5,
};