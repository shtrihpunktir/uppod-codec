"use strict";

exports.encode = encode;
exports.decode = decode;

function encode(str) {
    // данный код был был помечен как неиспользуемый при портировании с PHP на Node.js
    // const Iconv  = require("iconv").Iconv;
    // const unpack = require("hipack").unpack;
    // let iconv = new Iconv("UTF-8", "UTF-16BE");
    // let result = iconv.convert(str);
    // result = unpack("H*", result);

    let result = "";
    for (let i = 0; i < str.length; i++) {
        let decCode = str.charCodeAt(i);
        let hexCode = decCode.toString(16);
        hexCode = ("0000" + hexCode).slice(-4);
        result += hexCode;
    }

    result = result.replace(/0{1}([0-9a-f]{3})/gi, "$1");
    return "#" + result;
}

function decode(str) {
    let invalidStringMsg = "Invalid encoded string";
    if (!str.length) {
        throw Error(invalidStringMsg + ": should not be empty");
    }
    if (str[0] !== "#") {
        throw Error(invalidStringMsg + ": should start with '#' character");
    }
    if ((str.length - 1) % 3 !== 0) {
        throw Error(invalidStringMsg + ": should have length divided by 3");
    }

    let result = str.substr(1);

    let tmp = "";
    for (let i = 0; i < result.length / 3; i++) {
        let hexCode = result.substr(i * 3, 3);
        let decCode = parseInt(hexCode, 16);
        tmp += String.fromCharCode(decCode);
    }

    return tmp;
}