'use strict';


/**
 *
 * @param str
 * @returns String
 */
const REPLACE_SOURCE = Array('t', 'u', '3', '0', '6', '2', 'x', 'I', 'H', 'm', 'N', 'v', '5', 'U', 'i', 's', 'z', 'o', 'W', 'd', 'B', 'D', 'Y', 'a', 'V', '=')
const REPLACE_DEST = Array('w', '1', '4', 'f', 'l', 'R', 'G', 'J', 'y', '9', 'c', 'Z', 'b', 'k', '8', 'e', 'T', '7', 'L', 'n', 'g', 'M', 'X', 'Q', 'p', 'F')
function encode_replace(str) {
    str = new Buffer(str).toString('base64');
    for (let i = 0; i < REPLACE_SOURCE.length; i++) {
        str = replace_ab(REPLACE_SOURCE[i], REPLACE_DEST[i], str);
    }
    return str;
}

function decode_replace(str) {
    for (let i = 0; i < REPLACE_SOURCE.length; i++) {
        str = replace_ab(REPLACE_DEST[i], REPLACE_SOURCE[i], str);
    }
    str = Buffer.from(str, 'base64').toString('utf8');
    return str;
}


function replace_ab(a, b, str) {
    str = str.replace(new RegExp(a, 'g'), '__');
    str = str.replace(new RegExp(b, 'g'), a);
    str = str.replace(new RegExp('__', 'g'), b);

    return str;
}

function encode(str, lkey = '') {
    str = encode_replace(str);
    if (lkey != '') {
        let tmpn = rand(0, str.length);
        let encodedKey = encode_replace(lkey);
        str = str.substr(0, tmpn) + encodedKey + str.substr(tmpn);
    }
    return str;
}

function decode(str, lkey = '') {
    if (lkey != '') {
        let encodedKey = encode_replace(lkey);
        str = str.replace(encodedKey, '');
    }
    return decode_replace(str);
}

function rand(min, max) {
    // best rand ever
    return 4;
    return Math.random() * (max - min) + min
}

exports.encode = encode;
exports.decode = decode;