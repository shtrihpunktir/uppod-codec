# Uppod codec

Функции кодирования и декодирования ссылок для плеера [Uppod](http://uppod.ru).
 
Платформа предоставляет функции кодирования только для PHP. Этот проект — порт предоставляемых скриптов Uppod на Node.js.

## Установка

```
npm install https://bitbucket.org/shtrihpunktir/uppod-codec
```

## Использование
```javascript
var uppodCodec = require("uppod-codec");

var uriToEncode = "/foo/bar";
var encodedUriForHtml5 = uppodCodec.encodeUriForHtml5(uriToEncode); // #02f06606f06f02f062061072
var encodedUriForFlash = uppodCodec.encodeUriForFlash(uriToEncode); // 81AFJY9X8mH5ZvRWrH или что-то другое :)
```


